import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import PeoplePage from './src/pages/PeoplePage';
import PeopleDetailPage from './src/pages/PeopleDetailPage';

import { capitalizeFirstLetter } from './src/util';


const AppNavigator = createStackNavigator({
    'Main': {
        screen: PeoplePage
    },
    'PeopleDetail': {
        screen: PeopleDetailPage,
        navigationOptions: ({ navigation }) => {
            const username = UserProfile(navigation)
			const peopleName = capitalizeFirstLetter(
				username
			);
            return ({
                title: peopleName,
                headerTitleStyle: {
                    color: 'white',
                    fontSize: 30,
                }
            });
        }
    }
}, {    
    defaultNavigationOptions: {
        title: 'Pessoas!',
        headerTintColor: 'white',
        headerStyle: {
            backgroundColor: '#6ca2f7',
            borderBottomWidth: 1,
            borderBottomColor: '#C5C5C5'
        },
        headerTitleStyle: {
            color: 'white',
            fontSize: 30,
            alignSelf: 'center',
        }
    }
});

const AppContainer = createAppContainer(AppNavigator);

function UserProfile(navigation) {
    const perfil = navigation.state
    if (perfil.params != undefined) {
        return perfil.params.params.people.name.first
    }else{
        return perfil.key
    }
}

export default AppContainer;

