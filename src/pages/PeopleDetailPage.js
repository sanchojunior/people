import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import Line from '../components/Line';

export default class PeopleDetailPage extends React.Component {
    render (){
        return (  
            <View style={{ flex: 1}}>
                <User object={this.props}/>
                <UserDetail object={this.props}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 15
    },
    avatar: {
        aspectRatio: 1
    },
    detailContainer: {
        backgroundColor: '#e2f9ff',
        marginTop: 20,
        elevation: 1
    },
});

function User(props) {
    const object = props.object.navigation.state.params
    if (object != undefined){
        const people = object.params.people
        return <Image 
                    source={{ uri: people.picture.large }}
                    style={styles.avatar} 
                /> 
    }else{
        return <Text></Text>;
    }
}

function UserDetail(props) {
    const object = props.object.navigation.state.params
    if (object != undefined){
        const people = object.params.people
        return <View style={styles.detailContainer}>
                    <Line label="Email" content={people.email}/>
                    <Line label="Cidade" content={people.location.city}/>
                    <Line label="Estado" content={people.location.state}/>
                    <Line label="Tel" content={people.phone}/>
                    <Line label="Cel" content={people.cell}/>
                    <Line label="Nat" content={people.nat}/>
                </View>
    }else{
        return <Text></Text>;
    }
}